#!/usr/bin/python

__author__ = 'manuel.cabeza'

import sys


counter = 0
street = None
totalPerStreet = [0]*25

for line in sys.stdin:
    line = line.replace('\n', '').replace('[', '').replace(']', '').replace("'", '').replace(' ', '').split(',')

    try:
        if street != line[0]:
            if counter != 0:
                print street, map(lambda x: float(x)/counter , totalPerStreet[1:])

            street = line[0]
            counter = 0
            totalPerStreet = [0]*25

        if street == line[0]:
            totalPerStreet[0] = street
            for i, words in enumerate(line):
                if i == 0:
                    continue
                totalPerStreet[i] += int(words)
            counter += 1

    except ValueError:
        pass

print street, map(lambda x: float(x)/counter , totalPerStreet[1:])