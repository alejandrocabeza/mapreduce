#!/usr/bin/python

__author__ = 'manuel.cabeza'

import sys
import module

for line in sys.stdin:
    words = line.split(",")
    if len(words) == 31:
        wordlist = []
        wordlist.append(words[2])
        try:
            for word in words[module.BEGIN_TRAFFIC:module.SIZE]:
                wordlist.append(int(word.replace('\n', '')))
            print wordlist
        except ValueError:
            continue