#!/usr/bin/python

__author__ = 'manuel.cabeza'

import sys
import module

counter = 0
street = None
totalPerStreet = [0]*25
guideline = module.GUIDELINE.split(',')

for line in sys.stdin:
    line = line.replace('\n', '').replace('[', '').replace(']', '').replace("'", '').replace(' ', '').split(',')
    try:
        if street != line[0]:
            if counter != 0:

                highestTrafficInFrame = 0
                highestTrafficFrame = None

                for i, value in enumerate(totalPerStreet[1:]):

                    if value >= highestTrafficInFrame:
                        highestTrafficInFrame = value
                        highestTrafficFrame = i

                print street, guideline[module.BEGIN_TRAFFIC + highestTrafficFrame], highestTrafficInFrame


            street = line[0]
            counter = 0
            totalPerStreet = [0]*25

        if street == line[0]:
            totalPerStreet[0] = street
            for i, words in enumerate(line):
                if i == 0:
                    continue
                totalPerStreet[i] += int(words)
            counter += 1

    except ValueError:
        pass

highestTrafficInFrame = 0
highestTrafficFrame = None

for i, value in enumerate(totalPerStreet[1:]):
    if value >= highestTrafficInFrame:
        highestTrafficInFrame = value
        highestTrafficFrame = i

print street, guideline[module.BEGIN_TRAFFIC + highestTrafficFrame], highestTrafficInFrame