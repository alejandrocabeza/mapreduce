#!/usr/bin/python

__author__ = 'manuel.cabeza'

import sys

highestTraffic = 0
totalTraffic = 0
street = ""

for line in sys.stdin:
    line = line.replace('\n', '').replace('[', '').replace(']', '').replace("'", '').replace(' ', '').split(',')
    try:
        streetTraffic = sum(map(float,line[1:]))
        totalTraffic += streetTraffic

        if streetTraffic >= highestTraffic:
            street = line[0]
            highestTraffic = streetTraffic

    except ValueError:
        pass

print street, highestTraffic
print "TOTAL_TRAFFIC", totalTraffic