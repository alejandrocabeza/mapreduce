#!/usr/bin/python

__author__ = 'manuel.cabeza'

import sys

inTraffic = 0
outTraffic = 0

for line in sys.stdin:
    line = line.replace('\n', '').replace('[', '').replace(']', '').replace("'", '').replace(' ', '').split(',')
    try:
        streetTraffic = sum(map(float,line[2:]))

        if line[1] == "IN":
            inTraffic += streetTraffic
        else:
            outTraffic += streetTraffic

    except ValueError:
        pass

diff = inTraffic - outTraffic

if diff >= 0:
    print "IN_MORE_THAN_OUT", diff
else:
    print "OUT_MORE_THAN_IN", -diff