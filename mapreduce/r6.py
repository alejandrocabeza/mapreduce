#!/usr/bin/python

__author__ = 'manuel.cabeza'

import sys
import module

counter = 0
street = None
totalPerStreet = [0]*25
guideline = module.GUIDELINE.split(',')
roads = []

for line in sys.stdin:
    line = line.replace('\n', '').replace('[', '').replace(']', '').replace("'", '').replace(' ', '').split(',')
    try:
        if street != line[0]:
            if counter != 0:
                roads.append(totalPerStreet)

            street = line[0]
            counter = 0
            totalPerStreet = [0]*25

        if street == line[0]:
            totalPerStreet[0] = street
            for i, words in enumerate(line):
                if i == 0:
                    continue
                totalPerStreet[i] += int(words)
            counter += 1

    except ValueError:
        pass

roads.append(totalPerStreet)

for i, hour in enumerate(guideline[7:]):
    traffic = 0
    highestTrafficRoad = None

    for road in roads:
        if road[i + 1] >= traffic:
            traffic = road[i + 1]
            highestTrafficRoad = road[0]

    print hour, highestTrafficRoad


