#!/usr/bin/python

__author__ = 'manuel.cabeza'

import sys

roads = []

for line in sys.stdin:
    line = line.replace('\n', '').replace('[', '').replace(']', '').replace("'", '').replace(' ', '').split(',')
    street = [0]*26

    try:
        street[0] = line[0]
        street[1] = line[1]

        for i, words in enumerate(line):
            if i == 0 or i == 1:
                continue
            street[i] += int(words)

        if len(roads) == 0 or roads[0][0] == line[0]:
            roads.append(street)
        else:
            direction = {}
            for road in roads:
                dir = road[1]
                traff = sum(street[2:])

                if direction.has_key(dir):
                    direction[dir] += traff
                else:
                    direction[dir] = traff


            highestTrafficDirection = None
            traffic = 0


            for dir in direction:
                if direction[dir] >= traffic:
                    traffic = direction[dir]
                    highestTrafficDirection = dir

            print street[0], highestTrafficDirection, traffic

            roads = []

    except ValueError:
        pass